import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from "angularfire2/database";
import { User, AuthService } from './auth.service';
import { UserService } from './user.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-root',
    styleUrls: ['./app.component.scss'],
    template: `

    <nav class="navbar gather-nav">
    <a class="navbar-brand" routerLink=""><img src="/assets/images/logo_white.png" width="150" class="d-inline-block align-top" alt=""></a>
    <ul class="nav navbar-nav navbar-right">
      <li><a routerLink="">Home</a></li>
      <li *ngIf="user | async" ><a routerLink="user/{{ (isLoggedIn | async) ? 6 : 0 }}">Profile</a></li>
      <li *ngIf="user | async" (click)="signout()"><a href="#">Signout</a></li>
      <li *ngIf="hideLogin | async" id="register-li"><a routerLink="login">Log in</a></li>
    </ul>
    </nav>


  <div id="main-container" class="container-fluid">
    <router-outlet></router-outlet>
  </div>
    `
})
export class AppComponent implements OnInit {
    user: Observable<User | null>;
    isLoggedIn: Observable<User>;
    hideLogin: Observable<boolean>;

    constructor(
      private auth: AuthService,
      private userservice: UserService,
    ) {}

    ngOnInit() {
    this.user = this.auth.getCurrentUser();

      this.hideLogin = this.auth.getCurrentUser().pipe(
       map(x => x ? false: true) 
      );
      this.isLoggedIn = this.auth.isLoggedIn();
    }

    signout(): void {
      this.auth.signOut();
  }
}
