import { Component, OnInit } from '@angular/core';
import { User, AuthService } from "../../auth.service";
import { Event, EventService } from "../../event.service";
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Http } from '@angular/http';
import { RequestOptionsArgs } from '@angular/http';


export interface Topic {
    id: number,
    title: string,
}

@Component({
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})

export class UserFormComponent implements OnInit {
    user: Observable<User | null>;    
  userForm: FormGroup;
  newUser: boolean = true; // to toggle login or signup form
  passReset: boolean = false;
  topics: Topic[];
  eventIds: number[];
  imageUrl: string;
  options: RequestOptionsArgs;
  events$: Observable<Event[]>;
  // events: Observable<any>;
  

  constructor(private fb: FormBuilder, private es: EventService, private auth: AuthService, private http: Http) {}

   ngOnInit(): void {
     this.buildForm();
    this.user = this.auth.getCurrentUser();

    // this.es.getEventByUser(1).subscribe(console.log);
    // this.es.getById(1).subscribe(console.log);
    this.events$ = this.es.getFeaturedEvents();
   }

   toggleForm(): void {
     this.newUser = !this.newUser;
   }

   signup(): void {
    let u: User = {
      email: this.userForm.get('email').value,
      displayName: 'nameless user',
      imageUrl: this.imageUrl,
    }

     let creds = this.auth.emailSignUp(u, this.userForm.get('password').value)
        .then((creds) => {
            // let data = {

            // }
            // this.http.post('http://localhost:3004/users', )
            // .subscribe(res => {
            //     this.topics = res.json() as Topic[];
            //     console.log(this.topics);
            // });
        });
    console.log(creds);
   }

   login(): void {
     this.auth.emailLogin(this.userForm.get('email').value, this.userForm.get('password').value)
        .then((creds) => {
            console.log(creds);
            // this.http.post('http://localhost:3004/users', )
            // .subscribe(res => {
            //     this.topics = res.json() as Topic[];
            //     console.log(this.topics);
            // });
        });
   }

   signout(): void {
       this.auth.signOut();
   }

   onUploadDone(url: string){
       this.imageUrl = url;
   }

   resetPassword() {
     this.auth.resetPassword(this.userForm.value['email'])
     .then(() => this.passReset = true)
   }

   buildForm(): void {
     this.userForm = this.fb.group({
       'email': ['', [
           Validators.required,
           Validators.email
         ]
       ],
       'password': ['', [
         Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
         Validators.minLength(6),
         Validators.maxLength(25)
       ]
     ],
     });

     this.userForm.valueChanges.subscribe(data => this.onValueChanged(data));
     this.onValueChanged(); // reset validation messages
   }



   // Updates validation state on form changes.
   onValueChanged(data?: any) {
     if (!this.userForm) { return; }
     const form = this.userForm;
     for (const field in this.formErrors) {
       // clear previous error message (if any)
       this.formErrors[field] = '';
       const control = form.get(field);
       if (control && control.dirty && !control.valid) {
         const messages = this.validationMessages[field];
         for (const key in control.errors) {
           this.formErrors[field] += messages[key] + ' ';
         }
       }
     }
   }

  formErrors = {
     'email': '',
     'password': ''
   };

   validationMessages = {
     'email': {
       'required':      'Email is required.',
       'email':         'Email must be a valid email'
     },
     'password': {
       'required':      'Password is required.',
       'pattern':       'Password must be include at one letter and one number.',
       'minlength':     'Password must be at least 4 characters long.',
       'maxlength':     'Password cannot be more than 40 characters long.',
     }
   };

}