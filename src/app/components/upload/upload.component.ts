import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { map, tap, finalize } from 'rxjs/operators';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

interface Image {
    path: string;
    filename: string;
    downloadURL?: string;
    $key?: string;
}

@Component({
    selector: 'image-upload',
    template: `
  <h2>Upload a File</h2>
  <form ngNoForm>
  <input type="file" (change)="uploadFile($event)">
    </form>
    <h2>File Gallery</h2>
    <div style="overflow:hidden;">
        <div *ngFor="let img of imageList | async" style="position:relative;width:100px;height:100px;float:left;display:flex;justify-content:center;align-items:center;">
            <img [src]="img.downloadURL | async" style="max-width:100px;max-height:100px;">
            <button (click)="delete(img)" style="position:absolute;top:2px;right:2px;">[x]</button>
        </div>
    </div>
  `,
})

export class UploadComponent {
    @Input() folder: string = 'files/images';
    
    fileList : Observable<Image[]>;
    lists : Observable<{}[]>;
    imageList : Observable<Image[]>;
    uploadPercent: Observable<number>;
    downloadUrl: Observable<string>;


    @Output()
    url = new EventEmitter<string>();

    constructor(public af: AngularFireDatabase, public afs: AngularFireStorage, public router: Router) {
    }
    ngOnInit() {
      // console.log(this.af.database.ref(''));
      // this.lists = this.af.list('/files').valueChanges();
      
      // this.lists.subscribe(console.log);

      // let ref = this.afs.ref('files/images');
      // this.imageList = this.afs.storage..list(`/${this.folder}/images`).valueChanges().pipe(
      //   tap(console.log),
      //   map(x => x.map(
      //     image => {
      //         var pathReference = this.afs.ref(image.path);
      //         let result = {$key: image.$key, downloadURL: pathReference.getDownloadURL(), path: image.path, filename: image.filename};
      //         console.log(result);
      //         return result;
      //     }
      //   )),
      // );
    }


    // this.imageList = this.fileList.map( itemList =>
    //   itemList.map( item => {
    //       var pathReference = storage.ref(item.path);
    //       let result = {$key: item.$key, downloadURL: pathReference.getDownloadURL(), path: item.path, filename: item.filename};

    ngOnChanges() {
        console.log("new values for folder");
        // let storage = this.af.database;
        

        // console.log("Rendering all images in ",`/${this.folder}/images`)
      //   this.imageList = this.fileList.pipe(
      //     map( itemList =>
      //       itemList.map( item => {
      //           var pathReference = this.af.database.ref(item.path);
      //           let result = {$key: item.$key, path: item.path, filename: item.filename};
      //           console.log(result);
      //           return result;
      //       })
      //   )
      // );
    }


    uploadFile(event) {
      const file = event.target.files[0];
      console.log(file);
      const filePath = `${this.folder}/${file.name}`;
      const ref = this.afs.ref(filePath);
      const task = ref.put(file);

      this.uploadPercent = task.percentageChanges();
      // get notified when the download URL is available
      task.snapshotChanges().pipe(
          finalize(() => {
            console.log('Upload done!');
            this.downloadUrl = ref.getDownloadURL();
            this.downloadUrl.subscribe(x => {
                console.log('help');
                console.log(x);
                this.url.emit(x);
            });
           })
       )
      .subscribe();
    }



    // upload() {
    //     // Create a root reference
    //     let storageRef = this.af.database.ref();

    //     let success = false;
    //     // This currently only grabs item 0, TODO refactor it to grab them all
    //     for (let selectedFile of [(<HTMLInputElement>document.getElementById('file')).files[0]]) {
    //         console.log(selectedFile);
    //         // Make local copies of services because "this" will be clobbered
    //         let router = this.router;
    //         let af = this.af;
    //         let folder = this.folder;
    //         let name = selectedFile.name.split('.')[0];
    //         let path = `/${this.folder}/${selectedFile.name}`;
    //         var iRef = storageRef.child(`/${this.folder}/${name}`);
    //         iRef.update(selectedFile).then((snapshot) => {
    //             console.log('Uploaded a blob or file! Now storing the reference at',`/${this.folder}/images/`);
    //             af.list(`/${folder}/images/`).push({ path: path, filename: selectedFile.name })
    //         });
    //     }
    // }
    // delete(image: Image) {
    //     let storagePath = image.path;
    //     let referencePath = `${this.folder}/images/` + image.$key;

    //     // Do these as two separate steps so you can still try delete ref if file no longer exists
    //     // Delete from Storage
    //     this.af.database.ref().child(storagePath).delete()
    //     .then(
    //         () => {},
    //         (error) => console.error("Error deleting stored file",storagePath)
    //     );

    //     // Delete references
    //     this.af.object(referencePath).remove()
            
        

    // }
}