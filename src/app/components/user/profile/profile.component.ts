import { Component, OnInit, ChangeDetectionStrategy,ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { UserService } from '../../../user.service';
import { Event, EventService } from "../../../event.service";
import { Squad, SquadService } from "../../../squad.service";
import { Topic } from '../../users/user-form.component';

@Component({
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileComponent implements OnInit {
  user$: Observable<any[]| {}>;
  uid$: Observable<string>;
  isEditing = false;
  topics: Topic[];
  events$: Observable<Event[]>;
  squads$: Observable<Squad[]>;

  constructor(
    private userservice: UserService,
    private route: ActivatedRoute,
    private ref: ChangeDetectorRef,
    private es: EventService,
    private ss: SquadService,
  ) { }

  ngOnInit() {
    this.events$ = this.es.getEventByUser(1);
    this.squads$ = this.ss.getSquads();

    this.uid$ = this.route.params.pipe(
      map(params => params['uid']),
    );

    this.user$ = this.uid$.pipe(
      switchMap(uid =>
        this.userservice.getUser(uid),
      ),
    );
  }

}
