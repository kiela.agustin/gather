import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Event, EventService } from '../../../event.service';
import { UserService } from '../../../user.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  featuredEvents: Observable<Event[]>;
  users$: Observable<any[]| {}>;
  typewriterText: string = "ARE DOING";
  typewriterDisplay: string = "CAN DO";
  typeIndex = 0;

  constructor(
    private es: EventService,
    private userservice: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    // this.es.getById(1).subscribe(console.log);
    this.featuredEvents = this.es.getFeaturedEvents();

    this.users$ = this.userservice.getFeaturedUsers();
  }

  goToProfile(id: number) {
    this.router.navigate([`/user/${id}`]);
  }

  startType() {
    this.typingCallback(this);
  }

  typingCallback(that) {
    let totalLength = that.typewriterText.length;
    let currentLength = that.typewriterDisplay.length;

    if (currentLength > 6 && that.typeIndex === 0) {
      that.typewriterDisplay = 'CAN DO';
      setTimeout(that.typingCallback, 500, that);
      return;
    }

    if (that.typeIndex > 6) {
      that.typewriterDisplay += that.typewriterText[currentLength];
    } else {
      console.error(that.typewriterDisplay);
      that.typewriterDisplay = that.typewriterDisplay.slice(0, -1);
    }

    that.typeIndex += 1;

    if (that.typeIndex <= 15) {
      setTimeout(that.typingCallback, 200, that);
      return;
    }

    that.typeIndex = 0;
  }

}
