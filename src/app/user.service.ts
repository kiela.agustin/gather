import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface DbUser {
  id: number,
  name: string,
  description: string,
  position: string,
  isFeatured: boolean,
  photoUrl: string,
}

@Injectable()
export class UserService {

  private basePath = '/users';
  constructor(
    private db: AngularFireDatabase,
    private http: Http,
  ) { }

  addUser(data) {
    const obj = this.db.database.ref(this.basePath);
    obj.push(data);
    console.log('Successfully added user!');
  }

  getUser(uid: string): Observable<any[] | {}> {
    // return this.db.list(
    //     this.basePath,
    //     ref => ref.orderByChild('uid').equalTo(uid).limitToFirst(1),
    //   )
    //   .valueChanges()
    //   .pipe(map(users => users[0]));
    return this.http.get(`http://localhost:3004/user?id=${uid}`)
      .pipe(
        map(res => (res.json()[0] as DbUser)),
      );
  }

  getUsers(path): Observable<any[]> {
    return this.db.list(path).valueChanges();
  }

  getFeaturedUsers(): Observable<any[] | {}> {
    return this.http.get('http://localhost:3004/user?isFeatured=true')
      .pipe(
        map(res => res.json().map(user => user as DbUser)),
      );
  }
}
