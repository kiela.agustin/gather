import { RouterModule } from '@angular/router';
import { UploadComponent } from './components/upload/upload.component';
import { UserFormComponent } from './components/users/user-form.component';
import { HomeComponent } from './components/home/home/home.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { Routes } from '@angular/router';

export const appRoutes: Routes = [
  { path: 'upload',
    component: UploadComponent
  },
  { path: 'login',
    component: UserFormComponent
  },
  { path: 'user/:uid',
    component: ProfileComponent
  },
  { path: '',
    component: HomeComponent
  }
];
