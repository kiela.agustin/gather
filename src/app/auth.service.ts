import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { firebase } from '@firebase/app';
import { auth } from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import { UserService } from './user.service';

import { Observable, of } from 'rxjs';
import { switchMap, startWith, tap, filter, first } from 'rxjs/operators';

export interface User {
  uid?: string;
  email?: string | null;
  imageUrl?: string;
  displayName?: string;
}

export class EmailPasswordCredentials {
    email: string;
    password: string;
}

@Injectable()
export class AuthService {
  user: Observable<User | null>;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private userservice: UserService,
  ) {
    this.user = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
        }),
        // tap(user => localStorage.setItem('user', JSON.stringify(user))),
        // startWith(JSON.parse(localStorage.getItem('user')))
        // tap(console.log),
    );

    this.user.subscribe(console.log);
  }

  isLoggedIn() {
    return this.afAuth.authState.pipe(first())
  }

    getCurrentUser() {
        return this.user;
    }

    getUser(uid: string) {
      return this.afs.doc<User>(`users/${uid}`).valueChanges().pipe(
         tap(console.log) 
      );
  }
  //// Email/Password Auth ////
  emailSignUp(user: User, password: string) {
    return this.afAuth.auth
      .createUserWithEmailAndPassword(user.email, password)
      .then(credential => {
          let u = credential.user;
          const data: User = {
            uid: u.uid,
            email: u.email || null,
            displayName: u.displayName || 'nameless user',
            imageUrl: user.imageUrl || 'https://goo.gl/Fz9nrQ'
          };

          let dbUser = {
            uid: data.uid,
            name: data.displayName,
            email: data.email,
            age: null,
            location: null,
          };
          console.log('USER IN AUTH SERVICE: ', dbUser);
          this.userservice.addUser(dbUser);
        return this.updateUserData(data); // if using firestore
      })
      .catch(error => this.handleError(error));
  }

  emailLogin(email: string, password: string) {
    return this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(credential => {
        console.log('LOGGED IN');
        return this.updateUserData(credential.user);
      })
      .catch(error => this.handleError(error));
  }

  // Sends email allowing user to reset password
  resetPassword(email: string) {
    const fbAuth = auth();

    return fbAuth
      .sendPasswordResetEmail(email)
      .catch(error => this.handleError(error));
  }

  signOut() {
    this.afAuth.auth.signOut().then(() => {
        console.log('Signed out');
    //   this.router.navigate(['/']);
    });
  }

  // If error, console log and notify user
  private handleError(error: Error) {
    console.error(error);
  }

  // Sets user data to firestore after succesful login
  private updateUserData(user: User) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(
      `users/${user.uid}`
    );
    return userRef.set(Object.assign({}, user));
  }
}
