import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { map, tap, flatMap, mergeMap, first } from 'rxjs/operators';
import { OnInit } from '@angular/core';

export interface Event {
  id: number,
  title: string,
  description?: string,
  location?: string,
  date?: string,
  eventUrl?: string,
  photoUrl?: string,
  isFeatured?: boolean,
}

@Injectable()
export class EventService {

  private basePath = '/shares';
  userEventIds: Observable<number[]>;
  obs: Observable<any>;

  constructor(private http: Http) { }

  // getEvents(path): Observable<any[]> {
  //   return this.db.list(path).valueChanges();
  // }

  getById(id: number) {
    return this.http.get(`http://localhost:3004/events?id=${id}`).pipe(
      map(x => x.json() as Event)
    );
  }

  getFeaturedEvents() {
    return this.http.get(`http://localhost:3004/event?isFeatured=true`).pipe(
      map(x => x.json().map(y => y as Event)),
    );
  }

  getEventByUser(userId: number): Observable<any> {
    return this.http.get(`http://localhost:3004/user_event?uid=${userId}`).pipe(
        map( x => x.json().map(
          y => y.event_id
        )),
        mergeMap(ids => {
          const query = ids.reduce((query, id) => `${query}&id=${id}`, '');
          return this.http.get(`http://localhost:3004/event?${query}`);
        }),
        map(x => x.json().map(y => y as Event)),        
        first() 
      );
  }
}