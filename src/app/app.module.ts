
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { UserFormComponent } from './components/users/user-form.component';
import { appRoutes } from './router.module';
import { RouterModule } from '@angular/router';
import { ShareService } from './share.service';
import { SquadService } from './squad.service';
import { UserService } from './user.service';
import { AuthService } from './auth.service';
import { EventService } from './event.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UploadComponent } from './components/upload/upload.component';
import { LoginFormComponent } from './components/users/login-form/login-form.component';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { HttpModule } from '@angular/http';
import { HomeComponent } from './components/home/home/home.component';
import { ProfileComponent } from './components/user/profile/profile.component';

@NgModule({
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    TagInputModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
  ],
  declarations: [ UserFormComponent, AppComponent, UploadComponent, LoginFormComponent, HomeComponent, ProfileComponent],
  bootstrap: [ AppComponent ],
  providers: [ShareService, SquadService, AuthService, EventService, UserService]
})
export class AppModule {}
