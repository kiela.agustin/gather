import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Subject } from 'rxjs';
import { map, tap, flatMap, mergeMap, first } from 'rxjs/operators';

/// Notify users about errors and other helpful stuff
export interface Squad {
  id: number,
  name: string,
  description: string,
  memberCount: number,
}

@Injectable()
export class SquadService {

  constructor(private http: Http) { }
  
  getSquads() {
    return this.http.get(`http://localhost:3004/squad`).pipe(
      map( x => x.json().map(y => y as Squad)),
    );
  }

  
}