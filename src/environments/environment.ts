// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyC1Zu5Ao1tYaFsmjAPKC_M23JxqSxGz3iQ",
    authDomain: "gather-46567.firebaseapp.com",
    databaseURL: "https://gather-46567.firebaseio.com",
    projectId: "gather-46567",
    storageBucket: "gather-46567.appspot.com",
    messagingSenderId: "874538410426"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
